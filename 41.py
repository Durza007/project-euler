def p(n):
    b = 2.0
    while b < n:
        if (n/b) == int(n/b):
            return False
        else:
            if b==2.0:
                b=3.0
            else:
                b=b+2.0
    return True

largest = 0
def generate(n, A):
    global largest
    if n == 0:
        value = int(''.join(map(str, A)))
        if p(value):
            print(value)
            largest = max(largest, value)
    else:
        for i in range(0, n):
            generate(n - 1, A)
            if n % 2 == 1:
                j = 0
            else:
                j = i
            temp = A[j]
            A[j] = A[n - 1]
            A[n - 1] = temp

digits = [1, 2, 3, 4, 5, 6, 7]
generate(len(digits), digits)
print(largest)

print('Done.')
