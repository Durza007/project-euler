file = open('p022_names.txt')
names = file.read().replace('"', '').split(',')
file.close()

names.sort()

n = 0
sum = 0
for name in names:
    n += 1
    s = 0
    for c in name:
        s += ord(c) - ord('A') + 1

    sum += s * n

print(sum)

