import math

def getPen(n):
    return n * (3 * n - 1) / 2

def isPen(p):
    n = 1/6 + math.sqrt(p*2/3 + 1/36)
    return n == int(n)

D = 13333337
for j in range(1, 10000):
    for k in range(j + 1, 10000):
        pj = getPen(j)
        pk = getPen(k)

        if isPen(pj + pk) and isPen(pk - pj):
            D = min(D, abs(pk - pj))

print(D)
