def fac(n):
    if n == 1:
        return 1
    else:
        return n * fac(n - 1)

num = str(fac(100))

sum = 0
for i in num:
    sum += int(i)

print(sum)
