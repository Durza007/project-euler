def isPalindrome(string):
    for i in range(0, int(len(string) / 2)):
        if string[i] != string[-1 - i]:
            return False

    return True

sum = 0
for n in range(1, 1000000):
    d = format(n, 'd')
    b = format(n, 'b')

    if isPalindrome(d) and isPalindrome(b):
        sum += n

print(sum)
