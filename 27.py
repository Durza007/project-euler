import math
def is_prime(n):
    if n < 1:
        return False
    if n % 2 == 0 and n > 2: 
        return False
    return all(n % i for i in range(3, int(math.sqrt(n)) + 1, 2))

def quad(a, b):
    n = 0
    while is_prime(n**2 + a * n + b):
        n += 1

    return n

product = 0
l = 0
for a in range(-999, 1000):
    for b in range(-999, 1000):
        n = quad(a, b)
        if n > l:
            l = n
            product = a * b

print(product)
        
    
