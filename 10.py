import math

talAttTesta = 2000000
intePrimTal = [False] * talAttTesta # Array med alla tal - Punkt 1
antalPrimTal = 0 # Hur många intePrimTal vi hittat
tal = 3 # talet vi börjar med
rotenUrMax = math.sqrt(talAttTesta)

for loop in range(4, talAttTesta, 2): # punkt 2
    intePrimTal[loop] = True


while (tal < rotenUrMax): # Punkt 5
    if (intePrimTal[tal] == False): # Punkt 3
        for x in range(tal * 2, talAttTesta, tal): # Punkt 4
            intePrimTal[x] = True

    # Nästa tal kommer vara ett primtal - plussa på med 2, eftersom alla jämna tal redan är borta
    tal += 2

sum = 0
for loop in range(2, talAttTesta):
    if (intePrimTal[loop] == False):
        sum += loop

print(sum)
