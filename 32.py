def isPand(a, b, c):
    nums = [0] * 9

    string = str(a) + str(b) + str(c)
    if len(string) > 9:
        return False
    for c in string:
        if int(c) == 0:
            return False
        nums[int(c) - 1] += 1

    for n in nums:
        if n != 1:
            return False

    return True

sum = 0
sums = []
for a in range(1, 10000):
    for b in range(a, 10000):
        c = a * b
        if isPand(a, b, c) and not c in sums:
            sums.append(c)
            sum += c
            print(str(a) + " * " + str(b) + " = " + str(c) + ', SUM: ' + str(sum))

print(sum)
