def next(n):
    if n % 2 == 0:
        return n / 2
    else:
        return 3 * n + 1

l = 0
s = 0

for i in range(1, 1000000):
    n = i
    newL = 0
    while n != 1:
        n = next(n)
        newL += 1

    if newL > l:
        l = newL
        s = i

    if i % 10000 == 0:
        print(i)

print(s)
print(l)
