import math

file = open('p042_words.txt')
words = file.read().replace('"', '').split(',')
file.close()

def isTriangle(t):
    n = -0.5 + math.sqrt(t*2 + 0.25)
    return n == int(n)

num = 0
for word in words:
    sum = 0
    for c in word:
        sum += (ord(c) - ord('A') + 1)

    if isTriangle(sum):
       num += 1

print(num)
