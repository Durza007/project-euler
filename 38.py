def isPand(prods):
    nums = [0] * 9

    string = ''.join(prods)
    if len(string) != 9:
        return False
    for c in string:
        if int(c) == 0:
            return False
        nums[int(c) - 1] += 1

    for n in nums:
        if n != 1:
            return False

    return True

largest = 0
for n in range(1, 100000):
    digits = []
    for i in range(1, 1000):
        digits = digits + list(str(n * i))
        if len(digits) > 9:
            break

        if isPand(digits):
            largest = max(largest, int(''.join(digits)))

print(largest)
