digits = []
n = 1
while len(digits) < 1000000:
    digits += list(str(n))
    n += 1

prod = 1
for i in range(0, 7):
    prod *= int(digits[10**i - 1])

print(prod)
