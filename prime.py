import math

def sieve(talAttTesta):
    primtal = [True] * talAttTesta # Array med alla tal - Punkt 1
    antalPrimTal = 0 # Hur många intePrimTal vi hittat
    tal = 3 # talet vi börjar med
    rotenUrMax = math.sqrt(talAttTesta)

    for loop in range(4, talAttTesta, 2): # punkt 2
        primtal[loop] = False

    while (tal < rotenUrMax): # Punkt 5
        if (primtal[tal] == True): # Punkt 3
            for x in range(tal * 2, talAttTesta, tal): # Punkt 4
                primtal[x] = False

        # Nästa tal kommer vara ett primtal - plussa på med 2, eftersom alla jämna tal redan är borta
        tal += 2
    return primtal