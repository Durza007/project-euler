sum = 0
for n in range(2, 1000000):
    value = 0
    for d in str(n):
        value += int(d)**5

    if value == n:
        print(n)
        sum += n

print(sum)
