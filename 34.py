sum = 0

def fac(n):
    product = 1
    while n > 0:
        product *= n
        n -= 1
    return product

for n in range(10, 100000):
    facSum = 0
    for d in str(n):
        facSum += fac(int(d))

    if facSum == n:
        sum += n

print(sum)
