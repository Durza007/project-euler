def prime(n):
    b = 2.0
    while b < n:
        if (n/b) == int(n/b):
            return False
        else:
            if b==2.0:
                b=3.0
            else:
                b=b+2.0
    return True

print('start')
print(prime(15))
count = 1
n = 3

while count != 6:
    if prime(n) == True:
        count += 1
    n += 2

    if (count % 1000 == 0):
        print('.')

print(n - 2)

