coins = [1, 2, 5, 10, 20, 50, 100, 200]

t = 0
def search(n, sum):
    global t
    if n >= len(coins):
        if sum == 200:
            t += 1
        return

    if sum == 200:
        t += 1
        return

    if sum > 200:
        return

    for i in range(0, int(200 / coins[n]) + 1):
        search(n + 1, sum + coins[n] * i)


search(0, 0)
print(t)
