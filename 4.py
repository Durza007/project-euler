palindrome = 0

for x in range(100, 999):
    for y in range(100, 999):
        value = str(x * y)
        if (len(value) % 2 == 0):
            palin = True
            for i in range(0, len(value)/2):
                if (value[i] != value[len(value) - i - 1]):
                    palin = False

            if palin == True:
                if x*y > palindrome:
                    palindrome = x*y
        else:
            palin = True
            for i in range(0, len(value)/2):
                if (value[i] != value[len(value) - i - 1]):
                    palin = False

            if palin == True:
                if x*y > palindrome:
                    palindrome = x*y

print(palindrome)
