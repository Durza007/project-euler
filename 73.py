import prime

def prime_factors(n):
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    return factors

def gcd(a, b):
    aa = a
    bb = b
    t = 0
    while bb != 0:
        t = aa
        aa = bb
        bb = t % bb

    return aa

total = 12000
primes = prime.sieve(total)

def calc(d):
    low = 1/3
    high = 1/2
    count = 0
    for n in range(2, d + 1):
        #print("n: " + str(n))
        if n % 1000 == 0:
            print(n)
        for i in range(1, n):
            #print("i: " + str(i))
            if gcd(i, n) == 1:
                ##print("\t" + str(i) + "/" + str(n))
                val = i / n
                if low < val and val < high:
                    count += 1
    return count

print(calc(total))
