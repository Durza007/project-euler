def getNumPrimeFactors(value):
    b = 2
    factors = []
    while b <= value:
        if (value/b) == int(value/b):
            if not (b in factors):
                factors.append(b)
            value /= b
        else:
            if b==2:
                b=3
            else:
                b=b+2

    return len(factors)

n = 1
done = False
while not done:
    if n % 1000 == 0:
        print(n)
    
    for i in range(0, 4):
        if getNumPrimeFactors(n + i) != 4:
            n += i + 1
            break
        elif i == 3:
            done = True
            break

print(n)
