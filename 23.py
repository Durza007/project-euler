def getDivisors(n):
    list = [1]
    for i in range(2, int(n/2) + 1):
        if n % i == 0:
            list.append(i)

    return list

def isAbundant(n):
    return n < sum(getDivisors(n))

abun = []

for i in range(1, 28123):
    if isAbundant(i):
        abun.append(i)

def isSumAbund(n):
    global abun
    for x in range(0, len(abun)):
        if abun[x] > n:
            break
        for y in range(x, len(abun)):
            if abun[x] + abun[y] > n:
                break
            
            if abun[x] + abun[y] == n:
                return True

    return False

print('start search')
sum = 0
for i in range(1, 28123):
    if not isSumAbund(i):
        sum += i
    if i % 1000 == 0:
        print(int(i / 1000))

print(sum)
