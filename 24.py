perm = []

def generate(n, A):
    global perm
    if n == 0:
        perm.append(int(''.join(map(str, A))))
    else:
        for i in range(0, n):
            generate(n - 1, A)
            if n % 2 == 1:
                j = 0
            else:
                j = i
            temp = A[j]
            A[j] = A[n - 1]
            A[n - 1] = temp

generate(10, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
perm.sort()
print(perm[999999])

