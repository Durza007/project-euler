bestSolutions = 0
bestP = 0
for p in range(5, 1001):
    solutions = 0
    for a in range(1, p):
        for b in range(a, p - a):
            c = p - a - b
            if c < 1:
                break

            if a**2 + b**2 == c**2:
                solutions += 1

    if solutions > bestSolutions:
        bestSolutions = solutions
        bestP = p

print('p: ' + bestP)
print('Number of solutions: ' + bestSolutions)
