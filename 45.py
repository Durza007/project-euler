import math

def getTri(n):
    return n * (n + 1) / 2

def getPen(n):
    return n * (3 * n - 1) / 2

def getHex(n):
    return n * (2 * n - 1)

tn = 285
pn = 165
hn = 143

while True:
    tn += 1
    tri = getTri(tn)

    while tri > getPen(pn):
        pn += 1

    while tri > getHex(hn):
        hn += 1

    if tri == getPen(pn) and tri == getHex(hn):
        print(tri)
        break
