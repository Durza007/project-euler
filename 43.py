def p(n):
    b = 2.0
    while b < n:
        if (n/b) == int(n/b):
            return False
        else:
            if b==2.0:
                b=3.0
            else:
                b=b+2.0
    return True

def isSubStringDiv(string):
    divs = [2, 3, 5, 7, 11, 13, 17]
    for i in range(0, len(divs)):
        value = int(string[1 + i:1 + i + 3])
        if value/divs[i] != int(value/divs[i]):
            return False

    return True

sum = 0
def generate(n, A):
    global sum
    if n == 0:
        value = ''.join(map(str, A))
        if isSubStringDiv(value):
            sum += int(value)
    else:
        for i in range(0, n):
            generate(n - 1, A)
            if n % 2 == 1:
                j = 0
            else:
                j = i
            temp = A[j]
            A[j] = A[n - 1]
            A[n - 1] = temp

digits = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
generate(len(digits), digits)
print(sum)

print('Done.')
