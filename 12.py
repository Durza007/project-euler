import math

def getTri(n):
    sum = n
    for i in range(1, n):
        sum += i

    return sum

def isTri(n):
    sub = 1
    while n > 0:
        n -= sub
        sub += 1

    return n == 0

def divCount(x):
    s = 2
    for i in range(2, int(math.sqrt(x)) + 1):
        if (x % i == 0):
            s += 1
            if i != x / i:
                s += 1
    return s

n = 1
s = 1
l = n
record = 0
while True:
    d = divCount(s)
    record = max(record, d)
    if d > 500:
        print('>500: ' + str(s) + ', n = ' + str(n))

    n += 1
    s += n
    if n < l - 1000:
        l = n
        print(str(n) + ', s = ' + str(s) + ', RECORD: ' + str(record))

