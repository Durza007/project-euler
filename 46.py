import math

talAttTesta = 1000000
intePrimTal = [False] * talAttTesta # Array med alla tal - Punkt 1
antalPrimTal = 0 # Hur många intePrimTal vi hittat
tal = 3 # talet vi börjar med
rotenUrMax = math.sqrt(talAttTesta)

for loop in range(4, talAttTesta, 2): # punkt 2
    intePrimTal[loop] = True


while (tal < rotenUrMax): # Punkt 5
    if (intePrimTal[tal] == False): # Punkt 3
        for x in range(tal * 2, talAttTesta, tal): # Punkt 4
            intePrimTal[x] = True

    # Nästa tal kommer vara ett primtal - plussa på med 2, eftersom alla jämna tal redan är borta
    tal += 2

intePrimTal[1] = True

primes = []
for n in range(1, talAttTesta):
    if not intePrimTal[n]:
        primes.append(n)

squares = []
for n in range(1, 1000):
    squares.append(2*(n**2))

def isComp(n):
    global primes
    global squares
    for p in primes:
        if p >= n:
            return False
        for s in squares:
            if p + s > n:
                break
            if p + s == n:
                return True

n = 9
while True:
    n += 2
    if n in primes:
        continue
    if not isComp(n):
        print(n)
        break
            
