year = 1900
month = 1
day = 1
weekday = 1

lenM = [0,
        31,
        28,
        31,
        30,
        31,
        30,
        31,
        31,
        30,
        31,
        30,
        31]

sum = 0
while year <= 2000:
    day += 1
    weekday += 1

    if weekday > 7:
        weekday = 1

    limit = lenM[month]

    if month == 2:
        limit = 28
        if year % 4 == 0:
            limit = 29
        
        if year % 100 == 0 and year % 400 != 0:
            limit = 28

    if day > limit:
        day = 1
        month += 1

    if month > 12:
        month = 1
        year += 1

    if year >= 1901 and weekday == 7 and day == 1:
        sum += 1

print(sum)

    
