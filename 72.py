import prime

def prime_factors(n):
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    return factors

def gcd(a, b):
    aa = a
    bb = b
    t = 0
    while bb != 0:
        t = aa
        aa = bb
        bb = t % bb

    return aa

total = 1000000
primes = prime.sieve(total)

def calc(d):
    #print("start: " + str(d))
    count = 0
    for n in range(2, d + 1):
        #print("n: " + str(n))
        if n % 1000 == 0:
            print(n)
        if primes[n] == True:
            #print(" +" + str(n-1))
            count += n - 1
        else:
            for i in range(1, n):
                #print("i: " + str(i))
                if gcd(i, n) == 1:
                    ##print("\t" + str(i) + "/" + str(n))
                    count += 1
    return count


#print(calc(total))
#print("8: " + str(calc(8)))

print("d\tcount\tdiff\tfactors")
prev = 0
for i in range(1, 100):
    val = calc(i)
    diff = val - prev
    prev = val
    print(str(i) + "\t" + str(val) + "\t+" + str(diff) + "\t" +"*".join(map(str, prime_factors(val))))
