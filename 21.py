import math

def sOD(x):
  s = 1
  for i in range(2, int(math.sqrt(x)) + 1):
    if (x % i == 0):
      s += i
      if i != x/i:
          s += x / i
  return s

nums = [True] * 10000

sum = 0
n = 0
for i in range(0, len(nums)):
    if nums[i]:
        s = int(sOD(i))
        if int(sOD(s)) == i and s != i:
            print(str(i) + ' - ' + str(s))
            n += 1
            sum += s + i
            nums[i] = False
            nums[s] = False

print(sum)
print(n)
