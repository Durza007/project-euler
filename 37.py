import math

talAttTesta = 1000000
intePrimTal = [False] * talAttTesta # Array med alla tal - Punkt 1
antalPrimTal = 0 # Hur många intePrimTal vi hittat
tal = 3 # talet vi börjar med
rotenUrMax = math.sqrt(talAttTesta)

for loop in range(4, talAttTesta, 2): # punkt 2
    intePrimTal[loop] = True


while (tal < rotenUrMax): # Punkt 5
    if (intePrimTal[tal] == False): # Punkt 3
        for x in range(tal * 2, talAttTesta, tal): # Punkt 4
            intePrimTal[x] = True

    # Nästa tal kommer vara ett primtal - plussa på med 2, eftersom alla jämna tal redan är borta
    tal += 2

def p(n):
    global intePrimTal
    return not intePrimTal[n]

intePrimTal[1] = True
sum = 0
count = 0
for n in range(10, talAttTesta):
    if p(n):
        isTrunc = True
        for i in range(1, len(str(n))):
            if not p(int(str(n)[i:])) or not p(int(str(n)[:-i])):
                isTrunc = False
                break

        if isTrunc:
            print(n)
            sum += n

print(sum)
