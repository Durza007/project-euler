lower = [0, # zero
         3, # one
         3,
         5,
         4,
         4,
         3,
         5,
         5,
         4, # nine
         3, # ten
         6, # eleven
         6, # twelve
         8, # thirteen
         8, # fourteen
         7, # fifteen
         7, # sixteen
         9, # seventeen
         8, # eighteen
         8] # nineteen

mid = [0,
       0,
       len('twenty'),
       len('thirty'),
       len('forty'),
       len('fifty'),
       len('sixty'),
       len('seventy'),
       len('eighty'),
       len('ninety')]

def getTiotal(n):
    if n < 20:
        return lower[n]
    else:
        s = str(n)
        return mid[int(s[0])] + lower[int(s[1])]

def getHundratal(n):
    s = str(n)
    lowerCount = getTiotal(int(s[1] + s[2]))
    upperCount = lower[int(s[0])] + len('hundred')

    if lowerCount != 0:
        return upperCount + len('and') + lowerCount
    else:
        return upperCount

def getTal(n):
    if n < 100:
        return getTiotal(n)
    else:
        return getHundratal(n)

sum = 11
for i in range(1, 1000):
    sum += getTal(i)

print(sum)
